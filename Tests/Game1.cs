﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ComponentEngine2;

namespace Tests
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch           spriteBatch;

        Entity      player;
        OtherEntity otherPlayer;

        EntityWorld entityWorld;
        EntityWorld entityWorld2;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // Init entityWorld.
            entityWorld = new EntityWorld("Level 1");
            entityWorld2 = new EntityWorld("Level 2");

            // Setup new entities.

            // You don't have to directly use the entityWorld to create new entities if you don't want to.
            var notUnderWorld = new Entity(entityWorld, "NotUnderWorld");
            
            var player = entityWorld.SetupEntity("Player");
            var otherPlayer = entityWorld.SetupEntity("OtherPlayer");

            // NOTE: you can also do this without variable declaration,
            entityWorld.SetupEntity("NonVarEntity");
            // but if you do then you can access your entity like so:
            entityWorld.GetEntityFromTag("NonVarEntity");

            // Setup components.
            player.AddComponent(new Component(entityWorld, "Player"));
            otherPlayer.AddComponent(new Component(entityWorld, "OtherPlayer"));

            // Check that our component lists have been initialized.
            Console.WriteLine(player.Components);
            Console.WriteLine(otherPlayer.Components);

            foreach (var i in player.Components)      { Console.WriteLine(i); }
            foreach (var i in otherPlayer.Components) { Console.WriteLine(i); }

            // Make sure Components can have Owners which are derived from Entity, not ONLY Entity itself.
            // in my last project this was a problem due to the way I designed it :3
            var otherPlayerComponent = otherPlayer.GetComponent<Component>();
            Console.WriteLine(otherPlayerComponent.isActive);
            Console.WriteLine(entityWorld.GetEntityFromTag("Player").EntityWorld.Name);

            // You can move entities to and from entity worlds like so:
            entityWorld.MoveTo("Player", entityWorld2);
            Console.WriteLine(player.EntityWorld.Name);
            // And then to move them back:
            entityWorld2.MoveTo("Player", entityWorld);
            // This is more efficient than copying entities, as they are deleted upon being moved from one world to another,
            // it means that only 1 instance exists at a time.
            // This now outputs "Level 1" again:
            Console.WriteLine(player.EntityWorld.Name);

            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Loading... content...
        }
        
        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape)) { Exit(); }

            // To update, make sure all of your entities are registered under your EntityWorld,
            // and call this:
            entityWorld.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // The same is possible for drawing,
            // again, make sure all the entities you want to draw are registered under your EntityWorld,
            // and call:
            entityWorld.Update(gameTime);

            base.Draw(gameTime);
        }
    }
}
