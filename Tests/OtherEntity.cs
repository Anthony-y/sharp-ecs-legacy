﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ComponentEngine2;

namespace Tests
{
    public class OtherEntity : Entity
    {
        public OtherEntity(EntityWorld entityWorld, string tag)
            : base(entityWorld, tag)
        {
            Console.WriteLine("Hello from " + tag + " in world " + entityWorld);
        }
    }
}
