﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ComponentEngine2
{
    public class Component
    {
        //public Entity Owner { get; set; }
        public Entity Owner { get; set; }
        public bool isActive { get; set; } = true;
       
        public Component(EntityWorld entityWorld, string ownerTag)
        {
            Entity owner = entityWorld.GetEntityFromTag(ownerTag);
            if (owner != null) { Owner = owner; }
        }
        
        public virtual void Initialize() { CheckIsActive(); }
        public virtual void Update(GameTime gameTime) { CheckIsActive(); }
        public virtual void Draw(SpriteBatch spriteBatch) { CheckIsActive(); }
        public virtual void CheckIsActive() { if (!isActive) { return; } }
    }
}
