﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ComponentEngine2
{
    public class EntityWorld
    {
        public List<Entity> Entities { get; private set; }
        private string CurrentTag { get; set; }
        private Entity CurrentEntity { get; set; }
        public int MaxEntities { get; set; } = 300;
        public string Name { get; set; }
        private int DefaultCount { get; set; } = 0;

        public EntityWorld(string name)
        {
            Entities = new List<Entity>();
            Name = "Default_" + DefaultCount;
            DefaultCount++;
            Name = name;
        }

        public Entity SetupEntity(string entityTag)
        {
            if (Entities.Count < MaxEntities)
            {
                CurrentTag = entityTag;
                CurrentEntity = new Entity(this, entityTag);
                Entities.Add(CurrentEntity);
                return CurrentEntity;
            }
            else { Console.WriteLine("Could not create Entity. Amount of Entites exceeds maximum specified amount."); return null; }
        }

        public Entity SetupEntity(Entity entity)
        {
            if (Entities.Count < MaxEntities)
            {
                CurrentEntity = entity;
                CurrentTag = entity.Tag;
                Entities.Add(CurrentEntity);
                return CurrentEntity;
            }
            else { Console.WriteLine("Could not create Entity. Amount of Entites exceeds maximum specified amount."); return null; }
        }

        public TComponent GetComponent<TComponent>(string entityTag) where TComponent : Component
        {
            var match = GetEntityFromTag(entityTag).GetComponent<TComponent>();
            if (match != null) { return match; }
            else { Console.WriteLine("Could not retrieve component from Entity \"" + GetEntityFromTag(entityTag).Tag + "\""); return null; }
        }

        public Entity GetEntityFromTag(string tagToGet)
        {
            if (DoesEntityExist(tagToGet))
            {
                var match = Entities.FirstOrDefault(ent => ent.Tag == tagToGet);
                return match;
            }

            else
            {
                Console.WriteLine("Couldn't find Game Object of tag: " + tagToGet);
                return null;
            }
        }

        public bool DoesEntityExist(string name)
        {
            // Check if a game object in the list has the same tag as what was passed in.
            if (Entities.Any(ent => ent.Tag == name)) { return true; }
            else { return false; }
        }

        public void AddComponent(string entityTag, Component c)
        {
            var entity = GetEntityFromTag(entityTag);
            if (entity != null) { entity.AddComponent(c); }
            else { Console.WriteLine("Could not add Component to Entity. Could not find Entity."); }
        }

        public string GetTagFromEntity(Entity e)
        {
            var match = CurrentEntity.Tag;
            if (match != null) { return match; }
            else { Console.WriteLine("Could not get tag from entity. Entity is missing."); return null; }
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                Entities[i].Update(gameTime);
            }
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                Entities[i].Draw(spriteBatch);
            }
        }

        public void MoveTo(string entityTag, EntityWorld newEntityWorld)
        {
            var match = GetEntityFromTag(entityTag);
            if (match != null)
            {
                Entities.Remove(match);
                match.EntityWorld = newEntityWorld;
                newEntityWorld.Entities.Add(match);
                newEntityWorld.CurrentEntity = match;
                newEntityWorld.CurrentTag = match.Tag;
            }
            else { Console.WriteLine("Could not copy entity. Could not fine Entity \"" + match.Tag + "\""); return; }
        }

    }
}
