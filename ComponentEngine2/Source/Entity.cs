﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ComponentEngine2
{
    public class Entity
    {
        public string Tag { get; set; }
        public bool isActive { get; set; } = true;
        public List<Component> Components { get; set; }
        public EntityWorld EntityWorld { get; set; }

        public Entity(EntityWorld entityWorld, string tag)
        {
            EntityWorld = entityWorld;
            Components = new List<Component>();
            Tag = tag;
        }

        public void Update(GameTime gameTime)
        {
            if (!isActive) { return; }
            else
            {
                for (int i = 0; i < Components.Count; i++)
                {
                    Components[i].Update(gameTime);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!isActive) { return; }
            else
            {
                for (int i = 0; i < Components.Count; i++)
                {
                    Components[i].Draw(spriteBatch);
                }
            }
        }

        public void AddComponent(Component c) { Components.Add(c); }
        public void RemoveComponent(Component c)
        {
            var match = Components.Find(com => com.GetType().Equals(c));
            if (match != null) { Components.Remove(match); }
            else { Console.WriteLine("Error: could not remove component. Could not find component."); }
        }
        public T GetComponent<T>() where T : Component
        {
            var match = Components.Find(com => com.GetType().Equals(typeof(T)));
            if (match != null) { return (T)match; }
            else { Console.WriteLine("Could not retrieve component from Entity \"" + this.Tag + "\""); return null; }
        }

        public bool DoesComponentExist<T>() where T : Component
        {
            var match = Components.Find(com => com.GetType() == typeof(T));

            if (match != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
